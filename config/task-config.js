module.exports = {
  images      : false,
  fonts       : false,
  icons       : false,
  svgSprite   : false,
  stylesheets : true,
  javascripts : true,

  javascripts: {
    entry: {
      // files paths are relative to
      // javascripts.dest in path-config.json
      app: ["./app.js"],
      site: ["./site.js", "./modules/lazy.js"]
    },
    // This tells webpack middleware where to
    // serve js files from in development:
    publicPath: "/assets/js"
  },

  browserSync: {
    // Update this to match your development URL
    proxy: 'hampton.test',
    port: 3002,
    files: ['craft/templates/**/*']
  },

  production: {
    rev: true
  },

  ghPages     : false,
  html        : false,
  static      : false
}
