$(document).ready(function() {

  // Lazyloading google maps embed
  var gmap = $('.nav-contact-map iframe');
  $('a.nav-contact, .nav-page-contact').click(function() {
    console.log('clicked');
    gmap.attr("src", gmap.attr("data-src"));
    gmap.removeAttr('data-src');
  });

  $('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

  $('.nav__icons a').click(function(e) {
    e.preventDefault();
    var navClass = $(this).data('nav');
    $('.nav').addClass('switch');

    if( $(this).hasClass('nav-close') ) {
      $(this).removeClass('nav-close');
      $('.nav-offscreen').removeClass('active');
      $('body').removeClass('overflow');
      $('.nav--about').removeClass('hide');
    }

    else {
      $('body').addClass('overflow');
      $('.nav--about').addClass('hide');
      $('.nav__icons a').removeClass('nav-close');
      $(this).addClass('nav-close');
      $('.nav-offscreen').removeClass('active');
      $('.nav-offscreen.' + navClass).addClass('active');
    }

  });


  $('.nav-page-contact').click(function(e) {
    e.preventDefault();
    $('.nav-offscreen.nav-contact').addClass('active');
    $('.nav-offscreen.nav-pages').removeClass('active');
    $('.nav-pages').removeClass('nav-close');
    $('.nav-contact').addClass('nav-close');
  })

  $('body.home .hampton-way').click(function() {
      $('.nav-offscreen').removeClass('active');
      $('.nav__icons a').removeClass('nav-close')
      $('body').removeClass('overflow');
  })

  $('body.properties .search-properties').click(function() {
      $('.nav-offscreen').removeClass('active');
      $('.nav__icons a').removeClass('nav-close')
      $('body').removeClass('overflow');
  })

  jQuery(function($) {

    var $internalnav = $('.sticky-nav-internal');
    var $nav = $('nav.nav');
    var $win = $(window);
    var winH = $win.height();   // Get the window height.

    $win.on("scroll", function () {
      if ($(this).scrollTop() > 50 ) {
        $nav.addClass("switch");
        $internalnav.addClass("reveal");
      } else {
        $nav.removeClass("switch");
        $internalnav.removeClass("reveal");
      }
    }).on("resize", function(){ // If the user resizes the window
      winH = $(this).height(); // you'll need the new height value
    });

  });
})
