<?php

/**
* General Configuration
*
* All of your system's general configuration settings go in here.
* You can see a list of the default settings in craft/app/etc/config/defaults/general.php
*/

return array(
	'*' => array(
		'omitScriptNameInUrls' => true,
		'useEmailAsUsername' => true,
		'autoLoginAfterAccountActivation' => true,
		'defaultTokenDuration' => 'P4W',
		'maxUploadFileSize' => 54428800,
		'extraAllowedFileExtensions' => 'ase, ico, dmg, json, CWcode2SC',
		'userSessionDuration'           => 'P101Y',
		'rememberedUserSessionDuration' => 'P101Y',
		'rememberUsernameDuration'      => 'P101Y',
		'invalidLoginWindowDuration'    => 'P1W',
		'cooldownDuration'              => 'PT1M',
		'maxInvalidLogins'              => 10,
		'verificationCodeDuration' => 'P1M',
		'defaultSearchTermOptions' => array(
			'subLeft' => true,
			'subRight' => true,
		),
	),

	'.test' => array(
		'devMode' => true,
		'backupDbOnUpdate' => false,
		'enableTemplateCaching' => false,
		'siteUrl' => array(
			'en_us' => 'http://hampton.test/',
		),
    'environmentVariables' => array(
      'basePath' => '/users/tylerdeboer/sites/hampton/public/',
      'baseUrl'  => 'http://hampton.test/',
    )
	),

	'104.248.119.17' => array(
		'devMode' => true,
		'backupDbOnUpdate' => false,
		'sendPoweredByHeader' => false,
		'siteUrl' => array(
			'en_us' => 'http://104.248.119.17/',
		),
    'environmentVariables' => array(
      'basePath' => '/srv/users/serverpilot/apps/hampton/public/',
      'baseUrl'  => 'http://104.248.119.17/',
    )
	),

	'hampton1.com' => array(
		'backupDbOnUpdate' => false,
		'sendPoweredByHeader' => false,
		'siteUrl' => array(
			'en_us' => 'https://hampton1.com/',
		),
    'environmentVariables' => array(
      'basePath' => '/srv/users/serverpilot/apps/hampton/public/',
      'baseUrl'  => 'https://hampton1.com/',
    )
	),

);
