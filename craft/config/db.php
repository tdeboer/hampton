<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
	'*' => array(
		'tablePrefix' => 'craft',
	),

	'.test' => array(
		'server' => 'localhost',
		'user' => 'root',
		'password' => 'root',
		'database' => 'hampton',
	),

	'159.65.229.119' => array(
		'server' => '127.0.0.1',
		'user' => 'hampton',
		'password' => 'Hampton1!',
		'database' => 'craft',
	),

	'104.248.119.17' => array(
		'server' => '127.0.0.1',
		'user' => 'e4499f76ffd2',
		'password' => 'ef0d4fa39a243fa5',
		'database' => 'craft',
	),

	'hampton1.com' => array(
		'server' => '127.0.0.1',
		'user' => 'e4499f76ffd2',
		'password' => 'ef0d4fa39a243fa5',
		'database' => 'craft',
	),

);
